package com.stackOverFlow;

import java.time.Instant;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.rabbitmq.RabbitMQClient;
import io.vertx.rabbitmq.impl.RabbitMQClientImpl;

public abstract class DefaultQAService extends RestAPIVerticle {

	public static final String MONGO_PORT = ":27017";

	public static final String MONGODB_HOST = "MONGODB_HOST";

	protected static final int DEFAULT_PORT = 8090;

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start();

		Router router = Router.router(vertx);
		vertx.deployVerticle("com.stackOverFlow.QAReadVerticle",
				new DeploymentOptions().setWorker(true).setInstances(2));
		vertx.deployVerticle("com.stackOverFlow.QAPostVerticle",
				new DeploymentOptions().setWorker(true).setInstances(2));

		// body handler
		router.route().handler(BodyHandler.create());
		router.route().handler(CookieHandler.create());
		createSessionHandler(router);
		
		router.route().handler(CorsHandler.create("*")
			      .allowedMethod(HttpMethod.GET)
			      .allowedMethod(HttpMethod.POST)
			      .allowedMethod(HttpMethod.PUT)
			      .allowedMethod(HttpMethod.DELETE)
			      .allowedMethod(HttpMethod.OPTIONS)
			      .allowCredentials(true)
			      .allowedHeader("X-PINGARUNER")
			      .allowedHeader("Access-Control-Allow-Method")
			      .allowedHeader("Access-Control-Allow-Origin")
			      .allowedHeader("Access-Control-Allow-Credentials")
			      .allowedHeader("Authorization")
			      .allowedHeader("Content-Type"));

		router.get("/questions/").handler(rctx -> {
			JsonObject jsonObj = new JsonObject().put("MONGODB_HOST", getMongoDBHost());
			System.out.println("DefaultQAService.start() ALL QUESTIONS jsonObj: "+jsonObj.encodePrettily());
			vertx.eventBus().send("read.Question", jsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/questions/newest/").handler(rctx -> {
			JsonObject jsonObj = new JsonObject().put("MONGODB_HOST", getMongoDBHost());
			System.out.println("DefaultQAService.start() NEWEST QUESTIONS jsonObj: "+jsonObj.encodePrettily());
			vertx.eventBus().send("read.Question.newest", jsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

		router.get("/questions/results/").handler(rctx -> {
			System.out.println("DefaultQAService.start() SEARCH URI: " + rctx.request().uri());
			System.out.println("DefaultQAService.start() SEARCH search_query: " + rctx.request().getParam("search_query"));
			JsonObject jsonObj = new JsonObject().put("search_query", rctx.request().getParam("search_query"))
					.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.Question", jsonObj.encodePrettily(), res -> {
				System.out.println(
						"DefaultQAService.start() SEARCH res.succeeded(): " + res.succeeded() + " res.failed(): " + res.failed());
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					System.out.println("DefaultQAService.start() SEARCH res.cause(): " + res.cause());
					System.out.println("DefaultQAService.start() SEARCH FAILED: " + res.cause().getLocalizedMessage());
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

		router.get("/questions/:questionId").handler(rctx -> {
			System.out.println("DefaultQAService.start() BASED UPON questionId: " + rctx.request().getParam("questionId"));
			JsonObject jsonObj = new JsonObject().put("questionId", rctx.request().getParam("questionId"))
					.put("MONGODB_HOST", getMongoDBHost());
			vertx.eventBus().send("read.Question", jsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/questions/me/").handler(rctx -> {
			System.out.println("DefaultQAService.start() MY QUESTIONS");
			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() MY QUESTIONS FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for MY QUESTIONS!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/me/", new Handler<AsyncResult<Object>>() {

							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									System.out.println(
											"DefaultQAService.start(...).MY QUESTIONS new Handler() {...}.handle() statusCode: "
													+ statusCode);
									if (statusCode == 404) {
										JsonObject userObj = new JsonObject();
										userObj.put("username", usernameObj.getString("username"));
										userObj.put(MONGODB_HOST, getMongoDBHost());
										System.out.println("DefaultQAService.start() MY QUESTIONS userobj: "
												+ userObj.encodePrettily());
										vertx.eventBus().send("questions.me", userObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
		router.post("/questions/").handler(rctx -> {
			System.out.println("DefaultQAService.start() POST QUESTION");
			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() POST QUESTION FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for POST QUESTION!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {

							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									System.out.println(
											"DefaultQAService.start(...).POST QUESTION new Handler() {...}.handle() statusCode: "
													+ statusCode);
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										JsonObject userObj = new JsonObject();
										userObj.put("username", usernameObj.getString("username"));
										userObj.put("name", usernameObj.getString("name"));
										questionObj.put("user", userObj);
										questionObj.put("asked", Instant.now());
										System.out.println("DefaultQAService.start() POST QUESTION questionObj: "
												+ questionObj.encodePrettily());
										questionObj.put(MONGODB_HOST, getMongoDBHost());
										vertx.eventBus().send("post.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
//		router.patch("/questions/:questionId/edit/").handler(rctx -> {
		router.put("/questions/:questionId/edit/").handler(rctx -> {
			System.out.println("DefaultQAService.start() EDIT QUESTION");
			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() EDIT QUESTION FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for EDIT QUESTION!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								System.out.println("DefaultQAService.start() EDIT QUESTION result.result from authrorization(): "+result.result() + " instance of: "+(result.result() instanceof JsonObject));  
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									System.out.println("DefaultQAService.start() EDIT QUESTION statusCode from authrorization(): "+statusCode);
									if (statusCode == 404) {
										System.out.println("DefaultQAService.start() EDIT QUESTION rctx: "+rctx);
										JsonObject questionObj = rctx.getBodyAsJson();
										System.out.println("DefaultQAService.start() EDIT QUESTION questionObjjjj.encodePrettily(): "+questionObj.encodePrettily());
										System.out.println("DefaultQAService.start() EDIT QUESTION questionId: "+rctx.request().getParam("questionId"));
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put("lastUpdated", Instant.now());
										questionObj.put(MONGODB_HOST, getMongoDBHost());
										System.out.println("DefaultQAService.start() EDIT QUESTION questionObj.encodePrettily(): "+questionObj.encodePrettily());
										vertx.eventBus().send("edit.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
//		router.patch("/questions/:questionId/edit/").handler(rctx -> {
		router.put("/questions/:questionId/answer/:answerId/edit/").handler(rctx -> {
			System.out.println("DefaultQAService.start() EDIT ANSWER");
			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() EDIT ANSWER FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for EDIT ANSWER!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put("answerId", rctx.request().getParam("answerId"));
										questionObj.put("lastUpdated", Instant.now());
										questionObj.put(MONGODB_HOST, getMongoDBHost());
										vertx.eventBus().send("edit.answer", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
//		router.patch("/questions/:questionId/comment/").handler(rctx -> {
		router.put("/questions/:questionId/comment/").handler(rctx -> {
			System.out.println("DefaultQAService.start() COMMENT QUESTION");

			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() COMMENT QUESTION FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for COMMENT QUESTION!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										JsonObject userObj = new JsonObject();
										userObj.put("username", usernameObj.getString("username"));
										userObj.put("name", usernameObj.getString("name"));
										questionObj.put("user", userObj);
										questionObj.put("commented", Instant.now());
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put(MONGODB_HOST, getMongoDBHost());
										vertx.eventBus().send("comment.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
		router.put("/questions/:questionId/vote/").handler(rctx -> {
			System.out.println("DefaultQAService.start() VOTE QUESTION");

			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() VOTE QUESTION FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for VOTE QUESTION!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = new JsonObject();
										JsonObject userObj = new JsonObject();
										userObj.put("username", usernameObj.getString("username"));
										questionObj.put("user", userObj);
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put(MONGODB_HOST, getMongoDBHost());
										vertx.eventBus().send("vote.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
//		router.patch("/questions/:questionId/comment/").handler(rctx -> {
		router.put("/questions/:questionId/answer/:answerId/comment/").handler(rctx -> {
			System.out.println("DefaultQAService.start() COMMENT ANSWER");

			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() COMMENT ANSWER FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for COMMENT ANSWER!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										JsonObject userObj = new JsonObject();
										userObj.put("username", usernameObj.getString("username"));
										userObj.put("name", usernameObj.getString("name"));
										questionObj.put("user", userObj);
										questionObj.put("commented", Instant.now());
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put("answerId", rctx.request().getParam("answerId"));
										questionObj.put(MONGODB_HOST, getMongoDBHost());
										vertx.eventBus().send("comment.answer", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
//		router.patch("/questions/:questionId/answer/").handler(rctx -> {
		router.put("/questions/:questionId/answer/").handler(rctx -> {
			System.out.println("DefaultQAService.start() ANSWER TO QUESTION");
			getHttpClient(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("DefaultQAService.start() ANSWER TO QUESTIONFAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered for ANSWER TO QUESTION!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof JsonObject) {
									JsonObject usernameObj = (JsonObject) result.result();
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										JsonObject userObj = new JsonObject();
										userObj.put("username", usernameObj.getString("username"));
										userObj.put("name", usernameObj.getString("name"));
										questionObj.put("user", userObj);
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put("answered", Instant.now());
										questionObj.put(MONGODB_HOST, getMongoDBHost());
										vertx.eventBus().send("answer.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
		// Create the event bus handler which messages will be sent to
		vertx.eventBus().consumer("update.user.address", msg -> {
		  JsonObject json = (JsonObject) msg.body();
		  System.out.println("Got message: " + json.encodePrettily());
		  JsonObject userInfoObj = json.getJsonObject("body");
		  userInfoObj.put(MONGODB_HOST, getMongoDBHost());
			vertx.eventBus().send("update.user.info", userInfoObj.encodePrettily(), res -> {
				System.out.println("DefaultQAService.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					System.out.println("DefaultQAService.start() USER UPDATE SUCCEEDED");
				} else if (res.failed()) {
					System.out.println("DefaultQAService.start() FAILED cause: "+res.cause());
				}
			});
		});
		
//		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://35.190.147.51:5672"));
//		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://10.197.54.23:8090"));
		RabbitMQClient rabbit = new RabbitMQClientImpl(vertx, new JsonObject().put("uri", "amqp://104.196.128.2:8090"));
		rabbit.start(response -> {
			
			// Setup the link between rabbitmq consumer and event bus address
			rabbit.basicConsume("cmad.queue", "update.user.address", result -> {
				if (result.succeeded()) {
					System.out.println("DefaultQAService.start() result.result(): "+result.result());
					System.out.println("RabbitMQ consumer created !");
				} else {
					System.out.println("RabbitMQ consumer creation Failed");
					result.cause().printStackTrace();
				}
			});
		});
		
		createHttpServer(router, startFuture);
	}
	
//	private JsonObject getUserObject(String response) {
//		StringTokenizer tokenizer = new StringTokenizer(response, ":");
//		int statusCode = 0;
//		JsonObject userObj = null;
//		int count = 0;
//		while (tokenizer.hasMoreElements()) {
//			Object object = tokenizer.nextElement();
//			if (count == 0) {
//				statusCode = Integer.parseInt((String) object);
//			} else if (count == 1) {
//				userObj = (JsonObject) object;
//			}
//			count++;
//		}
//		userObj.put("statuscode", statusCode);
//		return userObj;
//	}
	
	protected JsonObject getUserDetails(int statuscode, JsonObject userObj) {
		userObj.put("statuscode", statuscode);
//		return statuscode + ":" + userObj.encodePrettily();
		return userObj;
	}

	protected abstract String getMongoDBHost();

	protected abstract void createSessionHandler(final Router router);

	protected abstract void getHttpClient(Handler<AsyncResult<Object>> handler);

	protected abstract void doAuthorization(RoutingContext context, HttpClient client, String path,
			Handler<AsyncResult<Object>> handler);
	
	protected abstract void createHttpServer(final Router router,Future<Void> future);

}
